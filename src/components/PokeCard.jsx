import { useContext, useEffect, useState } from "react"
import { AppContext } from "../App";
import usePokemonAPI from "../hooks/pokemon-api";

const PokeCard = ({url, selectedPokemons=[], setSelectedPokemons,setPokeInfo}) => {
    const [loading, setLoading] = useState(true);
    const [pokemon, setPokemon] = useState({});
    const pokemonAPI=usePokemonAPI();

    const {user} = useContext(AppContext);

    const shake = (element) => {
        element.classList.add('shake');
        setTimeout(()=>element.classList.remove('shake'),2000);
    }
    const setSelected = (event,pokemonId) => {
        if(user===''){
            return ;
        }
        const { target }= event;
        const element = (target.tagName==='DIV' && target.classList.contains('pokeCard')) ? 
            target : target.closest('.pokeCard');
        if(selectedPokemons.includes(pokemonId)){
            //element.classList.remove('selected');
            const axSelectedPokemons=selectedPokemons.filter((sp)=>sp!==pokemonId);
            setSelectedPokemons(axSelectedPokemons);
            return;
        }
        if(selectedPokemons.length < 6){
            //element.classList.add('selected');
            const axSelectedPokemons=selectedPokemons;
            axSelectedPokemons.push(pokemonId);
            setSelectedPokemons([...axSelectedPokemons]);
            return;
        }
        shake(element);
    }

    const setPokeInfoClick = (event,pokemon) => {
        event.stopPropagation();
        event.preventDefault();
        setPokeInfo(pokemon);
    }

    useEffect(()=>{
        const loadPokemon = () => {
            if(url){
                pokemonAPI.one(url).then((result)=>{
                    console.log(result);
                    setPokemon(result);
                    setLoading(false);
                });
            }
        }
        loadPokemon();
    },[url]);
    return(
        <>
            {!loading?
                <>
                    {(url !== undefined && url !==null) && 
                        <div className={`pokeCard ${selectedPokemons.includes(pokemon.id)?'selected':''}`} onClick={(event)=>setSelected(event,pokemon.id)}>
                            <img src={pokemon.sprites.front_default} alt={pokemon.name} />
                            <p className="title">{pokemon.name}</p>
                            <button className="displayInfo" onClick={(event)=>setPokeInfoClick(event,pokemon)}>
                                Detalles
                            </button>      
                        </div>
                    }
                </>
            :
                <></>
            }
        </>
    )
}

export default PokeCard