import axios from "axios";
import { useContext, useState } from "react";
import { AppContext } from "../App";
import Modal from "./Modal";
import PokemonsViewer from "./PokemonsViewer";
import UserChooser from "./UserChooser";

const MainView = () =>{
    const [selectedPokemons, setSelectedPokemons] = useState([]);
    const [newUser, setNewUser] = useState('');
    const [pokeInfo, setPokeInfo] = useState(null);

    const {user, setUser}=useContext(AppContext);

    const headers={
        'Accept': 'application/json',
        'Content-type': 'application/json'
    }

    const saveUserClick = (event) =>{
        if(newUser.trim()==='' || newUser.length<5 || newUser.length>200 || !/^[a-z]+$/i.test(newUser)){
            window.alert("Nombre de usuario no válido");
            return;
        }
        axios.post('http://localhost:8000/api/createUser',{user: newUser},{headers})
        .then((response)=>{
            setUser(response.data.user.user);
            setNewUser('');
            setSelectedPokemons([]);
            window.alert(response.data.message);
        })
        .catch((error)=>window.alert(error.response.data?.message || 'Error al guardar el usuario'))
    }

    return(
        <>
            <header className="bg-primary w-full h-20 flex flex-between">
                <div className="h-full w-0 flex-grow px-4 flex flex-start flex-wrap">
                    <UserChooser selectedPokemons={selectedPokemons} setSelectedPokemons={setSelectedPokemons} />
                </div>
                <div className="h-full w-0 flex-grow px-4 flex flex-end flex-wrap">
                    <input type="text" placeholder="Crear usuario" className=" h-8" value={newUser} onChange={(event)=>setNewUser(event.target.value)} />
                    <button className="btn btn-secondary text-primary px-4 h-8 mx-2" onClick={saveUserClick}>Crear Usuario</button>
                    <div className="w-full h-1/2">
                        <span>&nbsp;</span>
                        <span className="mx-4"></span>
                    </div>
                </div>
            </header>
            <PokemonsViewer total={150} selectedPokemons={selectedPokemons} setSelectedPokemons={setSelectedPokemons} setPokeInfo={setPokeInfo} />
            <Modal pokemon={pokeInfo} setPokemon={setPokeInfo} />
        </>
    )
};

export default MainView;