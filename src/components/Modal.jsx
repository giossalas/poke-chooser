import { useEffect } from "react";

const Modal = ({pokemon, setPokemon}) => {

    useEffect(()=>{
        console.log(pokemon);
    },[]);
    return(
        <>
            <div className={`modal ${(pokemon!==null)?'open':''} flex flex-center`}>
                <div className="pokeInfo">
                    {(pokemon!==null) &&
                    <>
                        <button className="close btn btn-danger text-white px-4" onClick={(event)=>setPokemon(null)}>Cerrar</button>
                        <img src={pokemon.sprites.front_default} alt={pokemon.name} />
                        <div className="text-center">
                            {pokemon.name}
                        </div>
                    </>
                    }
                </div>
            </div>
        </>
    )
}

export default Modal;