import { useContext, useEffect, useState } from "react";
import usePokemonAPI from "../hooks/pokemon-api";
import PokeCard from "./PokeCard";

const PokemonsViewer = ({total= 150, ...rest}) => {
    const [reload, setReload] = useState(true);
    const [pokemons, setPokemons] = useState([]);

    const pokemonAPI=usePokemonAPI();

    useEffect(()=>{
        const loadData = () => {
            pokemonAPI.get(total, 0).then((result)=>{
                const pokeResults=[].concat(pokemons).concat(result.results);
                setPokemons( pokeResults);
            });
        }
        if(reload){
            setReload(false);
            setPokemons([]);
            loadData();
        }
    },[reload]);

    return(
        <>
            <div className="w-full px-4 flex flex-start">
                <div className="pokeGrid w-full">
                    {pokemons.map((pokemon,index)=>(
                        <PokeCard key={index} url={pokemon.url} {...rest} />
                    ))}
                </div>
            </div>
        </>
    )
};

export default PokemonsViewer;