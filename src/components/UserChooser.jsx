import axios from "axios";
import { useContext, useState } from "react";
import { AppContext } from "../App";

const UserChooser = ({selectedPokemons=[],setSelectedPokemons}) => {
    const {user, setUser} = useContext(AppContext);
    const [inputUser, setInputUser] = useState('');

    const headers={
        'Accept': 'application/json',
        'Content-type': 'application/json'
    }

    const handleSearchUserClick = (event) => {
        if(inputUser.trim()===''){
            return;
        }
        axios.post('http://localhost:8000/api/getUserWithPokemons',
            {'user':inputUser},
            {
                headers
            }
        ).then((response)=>{
            setUser(response.data.user.user);
            setSelectedPokemons(response.data.pokemons.map((pokemon)=>pokemon.number_pokedex));
            setInputUser('');
        })
        .catch((error)=>window.alert(error.response.data?.message || 'Error al obtener la información'));
    };

    const handleSaveListClick = (event) => {
        if(selectedPokemons.length<6){
            window.alert("Seleccione 6 pokemones");
            return ;
        }
        axios.post('http://localhost:8000/api/saveUserPokemons',{user,pokemons:selectedPokemons},{headers})
        .then((response)=>{
           window.alert(response.data.message);
        })
        .catch((error)=>window.alert(error.response.data?.message || 'Error al guardar los datos'));
    }

    return(
        <>
            <input type="text" placeholder="Usuario a buscar" className=" h-8" value={inputUser} onChange={event=>setInputUser(event.target.value)} />
            <button className="btn btn-secondary text-primary px-4 h-8 mx-2" onClick={handleSearchUserClick}>Buscar usuario</button>
            <div className="w-full h-1/2 text-white text-sm">
                <span>Usuario: {user}&nbsp;&nbsp;&nbsp;</span>
                <span className="mx-4">Id de pokemones: [{selectedPokemons.join(', ')}] </span>
                {user!="" && <button className="btn btn-primary-light text-white h-8 px-4" onClick={handleSaveListClick} >Guardar lista</button>}
            </div>
        </>
    )
}

export default UserChooser;