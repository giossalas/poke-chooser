
const usePokemonAPI = () => {
    const url = 'https://pokeapi.co/api/v2/pokemon?';

    const shape = {
        get: (limit,offset) => {
            const pokeUrl=`${url}limit=${limit}&offset=${offset}`;
            return fetch(pokeUrl)
            .then((results)=>results.json());
        },
        one: (url) => fetch(url).then((results)=>results.json()),
    }; //shape
    return shape
};

export default  usePokemonAPI;