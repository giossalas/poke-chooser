import { createContext, useState } from 'react';
import RouterContainer from './routes/RouteContainer';
import './styles/App.scss';

export const AppContext = createContext();

function App() {
    const [user, setUser] = useState('');

    return (
        <AppContext.Provider value={{
            user,
            setUser
        }}>
            <div className="w-full h-screen bg-gray overflow-auto scroll-smooth">
                <RouterContainer />
            </div>
        </AppContext.Provider>
    );
}

export default App;
