import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import MainView from '../components/MainView';

const RouterContainer = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={MainView} />
            </Switch>
        </Router>
    )
};

export default RouterContainer;